data = [
    {
        "order_id": 85787,
        "warehouse_name": "хутор близ Диканьки",
        "highway_cost": -90,
        "products": [
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 3
            },
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 2
            },
            {
                "product": "билет в Израиль",
                "price": 1000,
                "quantity": 1
            }
        ]
    },
    {
        "order_id": 85788,
        "warehouse_name": "гиперборея",
        "highway_cost": 20,
        "products": [
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 3
            },
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 2
            },
            {
                "product": "билет в Израиль",
                "price": 134,
                "quantity": 3
            }
        ]
    }
]