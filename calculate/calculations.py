from typing import List
from pandas import DataFrame
from collections import defaultdict
from calculate.validate import (
    find_tariff_validate,
    order_stats_validate,
    order_profit_validate,
    warehouse_product_profit_validate
)


def find_tariff_per_warehouse(orders: List[dict]) -> dict:
    """calculation of the shipping cost rate for each warehouse"""
    warehouse_tariffs = defaultdict(int)
    for order in orders:
        find_tariff_validate(order)
        warehouse_name = order["warehouse_name"]
        total_quantity = sum(item["quantity"] for item in order["products"])
        tariff = order["highway_cost"]
        warehouse_tariffs[warehouse_name] += tariff * total_quantity
    return warehouse_tariffs


def calculate_order_stats(orders: List[dict]) -> dict:
    """Search total quantity, total revenue, total expense, and total profit for each item"""
    stats = defaultdict(lambda: {"quantity": 0, "income": 0, "expenses": 0, "profit": 0})
    for order in orders:
        order_stats_validate(order)
        for product in order["products"]:
            product_name = product["product"]
            quantity = product["quantity"]
            income = product["price"] * quantity
            expenses = quantity * order["highway_cost"]
            profit = income - expenses
            stats[product_name]["quantity"] += quantity
            stats[product_name]["income"] += income
            stats[product_name]["expenses"] += expenses
            stats[product_name]["profit"] += profit
    return dict(stats)


def calculate_order_profit(orders: List[dict]) -> List[dict]:
    """Calculation of the profit of the order, as well as the average income"""
    stats = []
    for order in orders:
        order_profit_validate(order)
        order_id = order["order_id"]
        order_profit = sum(
            (product["price"] - order["highway_cost"]) * product["quantity"] for product in order["products"])
        stats.append({"order_id": order_id, "order_profit": order_profit})
    return stats


def calculate_average_profit(orders: List[dict]) -> float:
    """Calculate average profit orders"""
    return sum(i["order_profit"] for i in orders) / len(orders)


# 4
def calculate_warehouse_product_profit(orders: List[dict]) -> List[dict]:
    """
    calculation of 'warehouse_name' , 'product','quantity', 'profit',
    and the percentage of the profit of a product ordered from a particular
    warehouse to that warehouse's profit
    """
    stats = []
    for order in orders:
        warehouse_product_profit_validate(order)
        for product in order["products"]:
            warehouse_name = order["warehouse_name"]
            quantity = product["quantity"]
            income = product["price"] * quantity
            expenses = quantity * order["highway_cost"]
            profit = income - expenses
            percent_profit_product_of_warehouse = (profit / (order["highway_cost"] * quantity)) * 100
            stats.append({
                "warehouse_name": warehouse_name,
                "product": product["product"],
                "quantity": product["quantity"],
                "profit": profit,
                "percent_profit_product_of_warehouse": percent_profit_product_of_warehouse
            }
            )
    return stats


def calculate_accumulated_percent_profit(df: DataFrame) -> DataFrame:
    """sorting 'percent_profit_product_of_warehouse' in descending order,
     then calculate the accumulated percentage. Accrued Interest"""

    df_sorted = df.sort_values(by="percent_profit_product_of_warehouse", ascending=False)
    df_sorted["accumulated_percent_profit_product_of_warehouse"] = df_sorted[
        "percent_profit_product_of_warehouse"].cumsum()
    df_sorted.reset_index(drop=True, inplace=True)
    return df_sorted


def assign_categories(df: DataFrame) -> DataFrame:
    """Assign A,B,C categories"""

    def categorize(percent):
        if percent <= 70:
            return "A"
        elif 70 < percent <= 90:
            return "B"
        else:
            return "C"

    df["category"] = df["accumulated_percent_profit_product_of_warehouse"].apply(categorize)
    return df
