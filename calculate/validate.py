def find_tariff_validate(order):
    """Validate for find_tariff_per_warehouse func, check order fields"""

    if "warehouse_name" not in order or "products" not in order or "highway_cost" not in order:
        raise ValueError("Invalid order data: Missing required keys.")
    if not order["products"]:
        raise ValueError("Invalid order data: Products list is empty.")


def order_stats_validate(order):
    """Validate for calculate_order_stats func, check order fields"""
    if "products" not in order:
        raise ValueError("Invalid order data: Missing 'products' key.")
    if not order["products"]:
        raise ValueError("Invalid order data: Products list is empty.")


def order_profit_validate(order):
    """Validate for calculate_order_profit, check order fields"""
    if "order_id" not in order or "products" not in order:
        raise ValueError("Invalid order data: Missing required keys.")
    if not order["products"]:
        raise ValueError("Invalid order data: Products list is empty.")


def warehouse_product_profit_validate(order):
    """Validate for calculate_warehouse_product_profit, check order fields"""
    if "warehouse_name" not in order or "products" not in order or "highway_cost" not in order:
        raise ValueError("Invalid order data: Missing required keys.")
    if not order["products"]:
        raise ValueError("Invalid order data: Products list is empty.")
