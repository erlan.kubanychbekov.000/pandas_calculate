# Order analysis

## How to start
For the microservice to work, you need a Python version of at least 3.10 and installed

Installing a virtual environment for the development environment on the example of Windows OS
```shell
python -m venv venv
venv\Scripts\activate
pip install -r req.txt
```
Installing a virtual environment for a production environment using Linux OS as an example
```shell
python -m venv venv
. venv/bin/activate
pip install -r req.txt
```

## Run the script
The script is launched in the activated local environment and in the root directory
```shell
python main.py
```