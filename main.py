import pandas as pd
from tabulate import tabulate
from data.order_data import data
from calculate.calculations import (
    calculate_average_profit,
    find_tariff_per_warehouse,
    calculate_order_stats,
    calculate_order_profit,
    calculate_warehouse_product_profit,
    calculate_accumulated_percent_profit,
    assign_categories,
)


def main():
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.float_format', '{:.2f}'.format)
    pd.set_option('display.max_colwidth', 100)

    orders_data = data

    warehouse_tariffs = find_tariff_per_warehouse(orders_data)
    warehouse_tariffs_df = pd.DataFrame(warehouse_tariffs.items(), columns=["warehouse_name", "tariff"])
    print("\n №1 Task shipping cost rate for each warehouse:")
    print(tabulate(warehouse_tariffs_df, headers="keys", tablefmt="grid"))

    order_stats = calculate_order_stats(orders_data)
    order_stats_df = pd.DataFrame(order_stats.values(), index=order_stats.keys())
    print("\n№2 Find the total quantity , total income , total expense and total profit for each product:")
    print(tabulate(order_stats_df, headers="keys", tablefmt="grid"))

    order_profit_stats = calculate_order_profit(orders_data)
    order_profit_stats_df = pd.DataFrame(order_profit_stats)
    average_profit = calculate_average_profit(order_profit_stats)
    df_average_profit = pd.DataFrame({"Average profit": [average_profit]})
    print("\n№3 Order profit:")
    print(tabulate(order_profit_stats_df, headers="keys", tablefmt="grid"))
    print(tabulate(df_average_profit, headers="keys", tablefmt="grid"))

    warehouse_product_profit_stats = calculate_warehouse_product_profit(orders_data)
    warehouse_product_profit_df = pd.DataFrame(warehouse_product_profit_stats)
    print("\n№4 Warehouse product profit:")
    print(tabulate(warehouse_product_profit_df, headers="keys", tablefmt="grid"))

    accumulated_percent_profit_df = calculate_accumulated_percent_profit(warehouse_product_profit_df)
    final_df = assign_categories(accumulated_percent_profit_df)
    print("\n№5 DataFrame with categories:")
    print(tabulate(final_df, headers="keys", tablefmt="grid"))


if __name__ == "__main__":
    main()
